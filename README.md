# COVID 19 DOOR OPENER [Work in Progress]


## Project presentation

<p align="center">
  <img src="https://gitlab.utc.fr/darnetju/covid19-door-opener/-/raw/master/Other%20documents/creo.jpg" width="450" alt="3D Preview">
</p>


Please read the attached .pdf presentation to discover this project. You will find all the basic information about our door opener such as what it looks like, how it works, why it has been created, and more.

## How to produce this door opener ?

This object is designed to be produced with plastic injection process, which allows us to produce a lot of pieces within a short period of time.
You can also built your door opener thanks to 3D-printers using PLA material. Please note that it takes around 9 hours to be entirely 3D-printed.

We are currently searching for a way to machine injection moulds to produce pieces in UTC's Labs.

## To do list

There are still some tasks to be done before producing this tool :

* [x] Finish the design
* [x] Make the caps able to be plastic injected
* [x] Check internal clearances
* [ ] Choose the materials
* [ ] Test the pieces on a standard 40mm PVC tube
* [ ] Generate moulds working planes
* [ ] Get ready with plastic injection !